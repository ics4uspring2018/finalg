import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the help button that brings you to the help screen
 * 
 * @Gary, Hayton, Wilson
 * @June 2018
 */
public class HelpButton extends Actor
{
    /**
     * sets the world to help screen
     */
    public void act() 
    {
         if(Greenfoot.mouseClicked(this))
        {
            Greenfoot.setWorld(new HelpScreen()); // Takes you to the help screen.
        }
    }    
}
