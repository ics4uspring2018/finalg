import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Man here.
 * 
 * @Gary, Hayton, Wlson
 * @version (a version number or a date)
 */
public class Man extends Actor
{
    private int jumpSpeed = -8;
    private int vspeed = 0;
    private int accelleration = 1;
    /**
     * Act - do whatever the Man wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    public void act() 
    {
        move();
        catchball();
        intersect();
    }    
    
    public void move()
    {
        Right(); 
        Left();
        //Jump();
    }
     public void Right()//method that allows character to move right when right arrow key is pressed
    {
        if(Greenfoot.isKeyDown("right")){
            setLocation(getX()+4,getY());
        }
    }

    public void Left()//method that allows character to move left when left arrow key is pressed
    {
        if(Greenfoot.isKeyDown("left")){
            setLocation(getX()-4,getY());
        }
    }
    public void Jump()//method that allows character to jump when up arrow key is pressed
    {
        if(Greenfoot.isKeyDown("Up")){
            vspeed=jumpSpeed-accelleration;
            fall();
    
        }
    }
    public void catchball()//method that allows player to catch ball and add score
    {
        Ball a = (Ball) getOneIntersectingObject(Ball.class);
        if(a!=null)
        {
            ((GameScreen)(getWorld())).score.point(1);
            
            getWorld().removeObject(getOneIntersectingObject(Ball.class));
        }
        
    }
     public void intersect()
    {
        Enemy enemy = (Enemy) getOneIntersectingObject(Enemy.class);
        if(enemy!=null)
        {
          Greenfoot.setWorld(new GameOver());
        }
        
    }
    public void fall()//allows player to fall when jumping
    {
        setLocation(getX(),getY()+vspeed);
        vspeed = vspeed+accelleration ;

    }
}
