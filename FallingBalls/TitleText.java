import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the title text class that displays the name of our game. 
 * 
 * @Gary, Hayton, Wlson
 * @June 2018
 */
public class TitleText extends Actor
{
    public TitleText()
    {
        setImage(new GreenfootImage("Basketb   ll Drop", 80, Color.RED,new Color(0,0,0,0)));
    }
    
    public void act() 
    {
        // Add your action code here.
    }    
}
