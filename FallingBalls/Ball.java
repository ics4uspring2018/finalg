import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
/**
 * Class for ball object that player has to catch
 * 
 * @Gary, Hayton, Wlson
 * 
 */
public class Ball extends Actor
{
    /**
     * Act - do whatever the Ball wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
       GameScreen world = (GameScreen) getWorld();
        if (((GameScreen) getWorld()).drop)
        {            
            setLocation(getX(), getY()+((GameScreen) getWorld()).dropSpeed);
            //drop the balloon after when certain amount of time
        }
        remove();
    }    
    
    public void remove()
    {
          
        
        if(getY() >= 390)
        {
            getWorld().removeObject(this);
            Greenfoot.setWorld(new GameOver());
            
            return;
            
        }
    }
    
    
}
