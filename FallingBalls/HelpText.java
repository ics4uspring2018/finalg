import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the instructions text
 * 
 * @Gary, Hayton, Wlson
 * @version (a version number or a date)
 */
public class HelpText extends Actor
{
    /**
     * Describes the game
     */
    public HelpText()
    {
        setImage(new GreenfootImage("Instructions: \n 1. Use arrow keys to move left and right \n 2. Collect as many basketballs as you can. \n 3. 1 point per ball collected  \n 4. Balls get faster as time goes on \n 5. You lose when a ball hits the floor.", 30, Color.WHITE,new Color(0,0,0,0)));
       
    }
}
