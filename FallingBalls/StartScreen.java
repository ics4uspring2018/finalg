import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Starting screen world
 * 
 * @Gary, Hayton, Wlson
 * @version (a version number or a date)
 */
public class StartScreen extends World
{

    /**
     * Constructor for objects of class StartScreen.
     * 
     */
    public StartScreen()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     * add title text, play button and help button
     */
    private void prepare()
    {
        TitleText titletext = new TitleText();
        addObject(titletext,163,65);
        titletext.setLocation(283,59);
        titletext.setLocation(282,56);
        titletext.setLocation(280,57);
        HelpButton helpbutton = new HelpButton();
        addObject(helpbutton,107,330);
        PlayButton playbutton = new PlayButton();
        addObject(playbutton,109,256);
        helpbutton.setLocation(109,329);
    }
}
