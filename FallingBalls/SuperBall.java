import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Extention of ball class, super ball. Is harder to catch and gives more points
 * 
 * @Gary, Hayton, Wlson
 * @version (a version number or a date)
 */
public class SuperBall extends Ball
{
    /**
     * Act - do whatever the SuperBall wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        GameScreen world = (GameScreen) getWorld();
        if (((GameScreen) getWorld()).drop)
        {            
            setLocation(getX(), getY()+((GameScreen) getWorld()).dropSpeed);
            //drop the balloon after when certain amount of time
        }
        remove();
    }
    
  
}
