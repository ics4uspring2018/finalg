import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is our score screen where we keep track of the scores
 * 
 * @Gary, Hayton, Wilson
 * @June 2018
 */
public class Score extends Actor
{
    public int score= 0 ;
    public void act() 
    {
        setImage (new GreenfootImage("Points: " + score, 30, Color.WHITE, new Color(0,0,0,0)));
    }    
    public void point(int x)
    {
        score = score + x;
    }
    public int getScore()
    {
        return score;
    }
    public void setScore(int x)
    {
        score = x;
    }
}
