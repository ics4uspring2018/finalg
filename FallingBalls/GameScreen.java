import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
/**
 * This is our game screen. 
 * 
 * @Gary, Hayton, Wilson 
 * @June 2018
 */
public class GameScreen extends World
{   
    public boolean drop = false;
    public int dropSpeed = 1;
    public int time = 0;
    public boolean exist=false;
    public static Score score;
    /**
     * Constructor for objects of class GameScreen.
     * 
     */
    public GameScreen()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
       super(600, 400, 1); 
       Man man = new Man();
       addObject(man,350,450);
       score = new Score();
       addObject(score,50,100); 
       
    }
    public void act()
    {
        spawnBall();
        spawnSuperBall();
        //spawnEnemies();
        time += 3;
        if (time % 15 == 0)
        {
            drop = true;
        }
        else
        {
            drop = false;
        }
        if (time % 1500 == 0 && dropSpeed < 12)
        {
            dropSpeed += 1;
        }
        //drop speed increase over time
        if (exist == false)
        { 
            ball(); 
        }
    }
    public void ball()//ball constructor
      {
        //int i = rand.nextInt(130);      
        //if (i == 1) {
        Ball ball = new Ball(); 
        addObject(ball,Greenfoot.getRandomNumber(400)+100, 0);
        exist = true;
        // add balloon when only when there isn't any
     }
    public void spawnBall() //spawns balls at random x coordinates
    {  
        Random rand = new Random();
        int x = rand.nextInt(300);
        Ball ball = new Ball();
        if (x == 1)
        {
         addObject(ball,Greenfoot.getRandomNumber(550), 0);   
        }
    }
    public void spawnSuperBall() //spawns superballs at random x coordinates
    {  
        Random rand = new Random();
        int x = rand.nextInt(3000);
        SuperBall ball = new SuperBall();
        if (x == 1)
        {
         addObject(ball,Greenfoot.getRandomNumber(550), 0);   
        }
    }
    public void spawnEnemies()//spawns enemies randomly at the bottom of screen
    {
       Random rand = new Random();
       int x = rand.nextInt(1000);
       Enemy enemy = new Enemy();
       if(x == 1)
       {
           addObject(enemy,600,450);
           Greenfoot.delay(5);
        }
      
    }
   
    
}
