import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the home button
 * 
 * @Gary, Hayton, Wilson
 * @June 2018
 */
public class GoHomeButton extends Actor
{
    public void act() 
    {
         if(Greenfoot.mouseClicked(this))
        {
            Greenfoot.setWorld(new StartScreen()); // When pressed, takes you back to the start screen
        }
    }    
}
