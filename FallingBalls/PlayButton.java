import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 *Play button 
 * 
 * @Gary, Hayton, Wilson
 * @June 2018
 */
public class PlayButton extends Actor
{
    public void act() //sets screen to game screen
    {
         if(Greenfoot.mouseClicked(this))
        {
            Greenfoot.setWorld(new GameScreen()); // This will take you to the game screen
        }
}
}
