import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Enemy class
 * 
 * @Gary, Hayton, Wlson
 * @version (a version number or a date)
 */
public class Enemy extends Actor
{
    /**
     * Act - do whatever the Enemies wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() //makes enemies run from right to left of bottom of screen
    {
       GameScreen world = (GameScreen) getWorld();
        if (((GameScreen) getWorld()).drop)
        {            
            setLocation(getX()-4, getY());
            //drop the balloon after when certain amount of time
        }
        remove();
        
    }      
    public void remove()//will get removed if it reaches the left side of screen
    {
          
        
        if(getX() <= 0)
        {
            getWorld().removeObject(this);
           // Greenfoot.setWorld(new GameOver();  
            return;
            
        }
    }
}
