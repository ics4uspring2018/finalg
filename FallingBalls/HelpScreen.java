import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Help screen world that shows instructions
 * 
 * @Gary, Hayton, Wlson
 * @version (a version number or a date)
 */
public class HelpScreen extends World
{

    /**
     * Constructor for objects of class HelpScreen.
     * adds instruction text and home button
     */
    public HelpScreen()
    {    
        super(600, 400, 1); 
        GoHomeButton gohome = new GoHomeButton();
        addObject(gohome,525,350);
        HelpText helptext = new HelpText();
        addObject(helptext,300,155);
    }
   
}
